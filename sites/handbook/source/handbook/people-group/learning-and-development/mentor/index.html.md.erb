---
layout: handbook-page-toc
title: Mentoring at GitLab
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## What is mentoring?

Mentor relationships are an opportunity for individuals to learn from someone's personal experience, background, and perspective. These relationships build trust on a team, provide safe space to make mistakes, and encourage both personal and professional development.

Mentorship is an opportunity for both the mentor and mentee to develop their leadership and communication skills. Mentorship should be led by the mentee, similar to how [1:1's at GitLab](/handbook/leadership/1-1/) are driven by direct reports.

As of 2021-10-25, 125 team members have participated in formal mentorship programs organized at GitLab. This includes 43 people in the Minorities in Tech mentorship program and 82 in the Women at GitLab mentorship program (first and second iteration combined).

### Organized Mentoring Programs at GitLab

<div class="flex-row" markdown="0">
  <div>
    <a href="/handbook/people-group/learning-and-development/mentor/company-program/" class="btn btn-purple" style="width:200px;margin:5px;">Company Wide Mentorship</a>
    <a href="/company/culture/inclusion/tmrg-gitlab-women/mentorship-program/" class="btn btn-purple" style="width:200px;margin:5px;">Women at GitLab Mentorship</a>
  </div>
</div>

***The Power of Mentoring***
<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/0W3d-PJ4-FM" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

### Questions

Share questions in the [#mentoring slack channel](https://app.slack.com/client/T02592416/C01QKNDJ76J).

## Benefits of mentorship

**Benefits for the mentee**

1. Be encouraged to prioritize, and be held accountable for, your [career development](/handbook/people-group/learning-and-development/career-development/)
1. Learn new skills related to your current role, your future career goals, or an area that you're passionate about
1. Set and reach clearly outlined [goals](/company/okrs/).

**Benefits for the mentor**

1. Serve as a [leader in the organization](/handbook/leadership/) by enabling growth for other team members
1. Practice leadership, [effective communication](/handbook/communication/), and [coaching](/handbook/leadership/coaching/) skills
1. Establish yourself as an expert in a field or speciality
1. [Build trust](/handbook/leadership/building-trust/) with team members

## Measuring Results of Mentorship

Consider these metrics when measuring success of individual, group, or formal mentorship programs.

1. Achievement of mentee goals
1. Satisfaction of mentors and mentees
1. Decision to maintain or end mentorship following initial timeframe
1. Career development and mobility in the 6-12 months following mentorship

### FY23 Q1 Results

During FY23 Q1, there were 2 mentorship programs running at GitLab. At the end of Q1, all mentees in current programs were asked to complete a short survey to report the affect their mentorship had on their achievement of business and personal goals during the quarter. 17% of mentees  completed the survey. Below are the results:

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSQo5yL0DOCgaAQWwXaIYHfb20pGIxlkzkxBB_xgU-rEsNkNa4U1NxevaCIQRZrgExrSQecEng50Kmy/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

**Additional Feedback Shared**

1. `Although not directly, but my mentor has indirectly helped me with my OKRs. My mentor has given me more confidence and knows-how in approaching my OKR goals.`
1. `With advice from my mentor, I was able to adjust the way I was reporting on my OKRs & projects to better communicate my progress. I also was coached on how to prepare myself for feedback & criticisms on my projects and how to better receive those comments - not taking them personally, and striving to be a part of the solution to fixing problems versus thinking I am the problem.`
1. `I have learnt to navigate GitLab code and improve my debugging skills, which directly feeds into delivering exceptional experience to my efficiency, customers results and skills development.`
1. `My mentor has been a force in my life. She is incredible - from helping me navigate new manager styles to how to prospect to how to deal with my mental health better - she's so so good. I hit the jackpot! I am in such a better place to actually work hard with her on my team, so the impact is indirectly directly related to my ability to sell.`




## Find a mentor

The following team members are available as mentors. Schedule a [coffee chat](/company/culture/all-remote/informal-communication/#coffee-chats) to get the conversation started!

The `Expertise` column comes from the team member .yaml entry and might not include all subjects they can help you with.

<%= mentors %>

## Become a mentor

1. Team members can designate their availability to be a mentor on the GitLab team page by setting the `mentor` status to `true`. This will appear on the team page with a line that reads `Available as a mentor`. [Example MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/75890)

## Organized mentorship programs

Learn more about organized mentorship programs for team members:

1. [Company-wide mentorship program](/handbook/people-group/learning-and-development/mentor/company-program)
1. [Minorities in Tech Mentoring Program](/company/culture/inclusion/erg-minorities-in-tech/mentoring/)
1. [Women's TMRG sponsored program](/company/culture/inclusion/tmrg-gitlab-women/mentorship-program/)
1. [Aspiring Manager mentorship pilot program](/handbook/engineering/development/dev/training/ic-to-manager/#aspiring-manager-mentorship-pilot-program)
1. The [Engineering department](/handbook/engineering/) outlines [strategies for mentors and mentees](/handbook/engineering/career-development/mentoring/) with suggestions on how to host meetings and set and evaluate goals.
1. The [Minorities in Tech TMRG](/company/culture/inclusion/erg-minorities-in-tech/) organized a [mentor program](/company/culture/inclusion/erg-minorities-in-tech/mentoring/) with the goal of enabling GitLab to support its Diversity value.
1. The [Sales department](/handbook/sales/) organized a pilot [Women in Sales Mentorship Program](/handbook/people-group/women-in-sales-mentorship-pilot-program/#women-in-sales-mentorship-program-pilot). The program benefits are outlined [here](/handbook/people-group/women-in-sales-mentorship-pilot-program/#program-benefits).
1. The [Support team](/handbook/support/) has outlined expectations and examples on [Mentorship in Support Engineering](/handbook/support/engineering/mentorship.html).
1. The [Finance team](/handbook/finance/) is running a [mentorship program](/handbook/finance/growth-and-development/mentorship).

Consider using the Minorities in Tech TMRG mentorship [program structure](/company/culture/inclusion/erg-minorities-in-tech/mentoring/program-structure/) to organize a mentor program for your team or TMRG.

## Expectations of mentors and mentees

Adapted from the [NCWIT Mentoring-in-a-Box Mentoring Basics - A Mentor's Guide to Success](https://www.ncwit.org/sites/default/files/legacy/pdf/IMentor_MentorGuide.pdf), section What Are the "Dos" of Mentoring and [People Grove resources](https://support.peoplegrove.com/hc/en-us/articles/360001265792-Structure-Goals-and-Agendas):

| Expectation | Description |
| ----- | --------------- |
| **Maintain Boundaries** | Maintain clear and appropriate boundaries. Be clear on where the line is drawn between your responsibilities and those of their manager. | 
| **Set Goals** | Mentorship is a process with a goal. Mentees should create goals for themselves from the outset and put them in writing. Frequently revisit goals to measure progress. | 
| **Build Trust** | Act as a colleague first, an expert second. Spend time [getting to know one another.](/handbook/values/#diversity-inclusion). Use an open and warm tone to create a safe place to ask difficult questions and take risks. [Be open, honest, and fully authentic.](/handbook/values/#transparency) | 
| **Set Expectations** | Be realistic and uphold your commitment. Share access to resources and people, but make it clear you do not wield your influence over others – coach as you can but the mentee needs to do their own work.| 
| **Listen** | Establish [trust](/handbook/leadership/building-trust/) and openness in communication from the start. Give your full, undivided attention. Listen as much as you speak so that their questions and aspirations are always the central focus. Hear concerns before offering advice and guidance.| 
| **Acknowledge Independence** | Recognise that the mentee goals are their own and that they may have career goals that differ from the path you chose. Your role as a mentor is to guide; it’s up to the mentee to decide what to implement in their own career. | 
| **Respect Diverse Experiences** | Recognise that minorities within the organisation sometimes face additional barriers to advancement. Educate yourself about the issues and ask for advice and support via the [appropriate Diversity, Inclusion and Belonging channels](/company/culture/inclusion/erg-guide/#how-to-join-current-tmrgs-and-their-slack-channels). | 
| **Practice Self-Awareness** | Identify your strengths, weaknesses, and [biases.](/handbook/values/#unconscious-bias). Keep an open mind. Be aware of and respect other's experiences, ideas, and goals. Mentorship requires open dialogue about the ways gender and culture influence your mentee's work in the organisation and the mentoring relationship itself. | 
| **Advocate** | Educate others within the organisation about the advancement of women and other under-represented groups. Approach managers and other team members and mentor them on being effective managers or colleagues to those who might have different experiences to them. Teach your mentee how to become a mentor themselves – by example and by encouragement.| 
| **Express Gratitude** | Share openly about your experience. Express gratitude for the skills or experiences developed over time. | 
| **Maintain a Growth Mindset** |  Value a [growth mindset](/handbook/values/#growth-mindset) of both the mentor and mentee. Both parties can learn and grow from mentorship| 
| **Iterate** | Mentorship changes with time. [Iteration](/handbook/values/#iteration) of goals throughout the mentorship is encouraged. Focus on quick wins that the mentee can reach, then keep growing |
| **Confidentiality** | Mentorship should respect the trust built between mentor and mentee. Challenges and conversations shared in mentorship sessions should be kept confidential unless the mentor and mentee are OK with them being shared |

## Resources

Use these resources when participating in an organized mentor program or building your own mentor/mentee relationship in both synchronous and asynchronous formats. Adopt what fits and leave what doesn't.

### Mentor and Mentee training

Take the LinkedIn Learning training called [How to be a Good Mentor and Mentee](https://www.linkedin.com/learning/how-to-be-a-good-mentee-and-mentor/the-power-of-mentoring?u=2255073). It covers strategies for finding a mentor, setting clear expectations, and achieving goals through mentorship. If you don't have a LinkedIn Learning license, please submit an access request and assign to the L&D team. The team will help either allocate a license to your account or direct you to the [Growth and Development budget](/handbook/total-rewards/benefits/general-and-entity-benefits/growth-and-development/) for reimbursement.

Content from this training has been adapted in the following [Google Slides presentation](https://docs.google.com/presentation/d/1QPx9ZGa051Jhwwfb78cKW1LD0uVTUdKxRdElBk4Ku9I/edit?usp=sharing) for learners who prefer to read the material at their own pace.

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vS3cZJcCIv_s44OfN9QLjje2wRqS7EwnrK3HCS_ZeT-ZGwk58hPq17L-c_DvCdvu0jxjR3r6yY8xY79/embed?start=false&loop=false&delayms=60000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

#### Mentee: What to do before your mentorship begins

1. Set up a coffee chat with your mentor. Take time to get to know them and [build trust](/handbook/leadership/building-trust/). Discuss communication styles and preferences
1. Set a specific goal to achieve during your mentorship. It's OK if you don't reach 100% of your goal during the mentorship.
1. Watch this short LinkedIn video on [understanding strengths and weaknesses](https://www.linkedin.com/learning/management-tips/understanding-strengths-and-weaknesses?u=2255073)
1. Take the [16 Personalities test](https://www.16personalities.com/free-personality-test) and share the results with your mentor/mentee. This can help set expectations for effective communication.
1. Join the [#mentoring](https://app.slack.com/client/T02592416/C01QKNDJ76J/thread/C5P8T9VQX-1587584276.009700) Slack channel

### Set your goal and purpose

In their book [Burnout: the secret to unlocking the stress cycle](https://www.burnoutbook.net/), authors Emily and Amelia Nagoski explain the difference between goals and purpose:

| Idea | Definition |
| ----- | --------------- |
| Goal | the `what` - outcome driven |
| Purpose | the `why` - value driven |

Mentees should have a foundational goal and purpose set for their mentorship. **Mentee goals should drive the relationship.**. Mentors can set goals based on that of their mentee. Both mentors and mentees bring their own original purpose to mentorship. 

#### What is your goal?

| Critera | Description |
| ----- | --------------- |
| Time-bound | When will the goal be complete? |
| Certain | Is achievement of the goal within your control? |
| Specific | What is the desired outcome? |
| Positive | What positive impact will the outcome create? |
| Measurable | How will success be measured? |
| Personal | Why does achieving the goal matter? Tie goal to purpose. |

Using the matrix below, fill in each empy space based on the descriptions above to draft your goal:

| Time-bound | Certain | Specific | Positive | Measurable | Personal |
| -------- | -------- | -------- | -------- | -------- | -------- | 
| | | | | | |

_This structure is adapted from [Burnout: the secret to unlocking the stress cycle](https://www.burnoutbook.net/) and the [SMART goal framework](https://support.peoplegrove.com/hc/en-us/articles/360001265792-Structure-Goals-and-Agendas). Learn more about goal setting by taking the [Setting Team and Employee Goals using SMART Methodology LinkedIn Learning course](https://www.linkedin.com/learning/setting-team-and-employee-goals-using-smart-methodology/how-to-use-smart-goals-2?u=2255073)_

#### Setting tool-specific goals

Setting goals that are specific to learning a specific tool, like increasing confidence with GitLab, might be challenging to fit in the SMART model. Consider asking yourself these two questions to help clarify your goal. 

1. What will you do to improve your understanding of the GitLab product?
1. How will you show that you have improved your understanding of the GitLab product / what do you expect to be able to do?

Then, try breaking down your goal into clear actions as shown in the examples below:

> Example 1: Improve my understanding of GitLab product by X date
>     1. Move all personal projects to GitLab
>     1. Configure CI/CD tools to automate 1 manual task
>     1. Track career development in a personal project

> Example 2: Learn about Ruby by X date
>     1. Take X course
>     1. Resolve 3 ruby bugs in the product
>     1. Create 1 blog post with learnings

#### What is your purpose?

Purpose is value-driven and based on [GitLab values](/handbook/values/) and personal values. Determine what values drive you to be part of a mentorship and complete the following statement:

**I'm a `mentor/mentee` because I care about `value`. `Value` drives me to be a `mentor/mentee` because `your why`.**

**Connecting mentorship and GitLab values**

| Value | Connection to mentorship |
| ----- | --------------- |
| Collaboration | Fosters connection and conversation between team members |
| Results | Driven by specific goals |
| Efficiency | Focused transfer of experience and skill |
| Diversity, Inclusion, and Belonging | All perspecives, experiences, and backgrounds are valued and included |
| Iteration | Incremental changes to goals are made as relationships develop |
| Transparency | Trust is built between mentor/menee |

#### Communicate your goal and purpose

A discussion about goals and purpose should be part of the very first mentorship meeting. Progress and iterations on goals should be revisited during each mentorship meeting.

### Setting a Meeting Schedule

It's important to set clear expectations with your mentor/mentee about when, how often, and for how long you will formally meet. Below is a suggested format for this meeting cadence:

- Establish a window for your mentor/mentee relationship. 3-6 month is suggested
- Set up a meeting cadence every other week for a 30-45 minute time period or agree to spend 30-45 minutes every other week contributing to async communication formats
- When the mentorship month window concludes, set expectations for how your mentorship will continue. Some options include:
     - Agree on the cadence you'll meet moving forward either synchronously or asynchronously
     - Decide to end the mentorship and consider staying connected via coffee chats or informal messages on Slack

### Sample sync meeting agendas

Setting an agenda for your mentorship sessions is important for resource documentation and future planning. The mentee should be the DRI for each session and use the agenda to set meeting goals and ask questions. Open a new document and share the document with both the mentor/mentee. Use the following meeting agenda templates as a baseline for each session. The template can be customized to meet the needs of the relationship, and might adapt over time.

Sample agendas below are inspired by resources from [Arizona State University](https://wms.arizona.edu/sites/default/files/Sample%20Meeting%20Agendas%20and%20Emails%20for%20Mentors.pdf) and [People Grove](https://support.peoplegrove.com/hc/en-us/articles/360001265792-Structure-Goals-and-Agendas). Please make a copy of these templates and save to your own Google Drive.

1. [Initial Meeting Agenda Template](https://docs.google.com/document/d/1p1jVyE6S2qvCiNwjdwhREj4ge1AGneb0pFxfC3T2rPY/edit?usp=sharing)
1. [Regular Meeting Agenda Template](https://docs.google.com/document/d/1ufU9Hjcuf2-psASdXS_B7zX64C-5eBuvOR-P7pbSULM/edit?usp=sharing)
1. [Final Meeting Agenda Template](https://docs.google.com/document/d/1IVwilfvUN-XE6FE1kjKLLmou4YUQYDD8ba57kM7dyGY/edit?usp=sharing)

### Sample async meeting formats

Watch this short video of how you can use GitLab issues to collaborate async in a mentorship:

<div style="position: relative; padding-bottom: 62.5%; height: 0;"><iframe src="https://www.loom.com/embed/eb29c7d889ce42a396775547f8c83df1" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

You can explore the issue shown in this video in our [GitLab L&D project](https://gitlab.com/gitlab-com/people-group/learning-development/mentorship/-/issues/8). There is also a [issue template](https://gitlab.com/gitlab-com/people-group/learning-development/mentorship/-/issues/7) you can copy and use in your own mentorship project. The text of the issue template is also included directly below.

***GitLab Issue Template***

```
## Date: XXXX-XX-XX

`Add mentorship goal as the first entry on the agenda. Keep this entry here the entirety of the mentorship`

## Questions and Feedback by 20xx-xx-xx

_Mentees should add questions and links to projects for feedback. Include links to issues and MRs that you need guidance on_

1. [ ]  a
1. [ ]  a
1. [ ] a

## Action Items

_Use this section to queue up action items for the mentee and mentor. Check each box when complete._

**1. Mentee Action Items**
   1. [ ] a
   1. [ ] a
   1. [ ] a

**1. Mentor Action Items**
   1. [ ] a
   1. [ ] a
   1. [ ] a

## Maintenance

1. [ ] Assign this issue to both the mentor and mentee
1. [ ] Use the comment section on this issue to collaborate with your mentor async. Include links to Loom video recordings in your comments if you need to verbalize or explain something in more detail.
1. [ ] Set a due date on this issue when the feedback and actions items should be complete
1. [ ] Close this issue when all feedback is addressed and action items are checked off

```

### Ending your mentorship

Mentorships end or change over time. Review these guidelines for how to end your mentorship with grace and some suggested actions to take as your mentorship ends.

1. It's OK for a mentorship to end if the mentor and mentor and mentee feel they've accomplished the mentorship goal or if the pair wasn't a good fit. Consider having a coffee chat with your mentor/mentee to discuss what was accomplished or what didn't work. Thank each other for taking time for the mentorship and perhaps suggest a new mentor/mentee that you know who would benefit from their support.
1. Mentorship doesn't have to end, either! Once you've achieved your goal or reached the end of your program, determine a cadence to stay connected with your mentor. You might meet 1x per month for a coffee chat, connect async via Slack, or continue with your 2x monthly sessions.
1. Share the results of your mentorship with your manager. If you're a manager, share the what you learned with your direct reports. Take time in your 1:1s to share why and how the mentorship helped you reach your goals, either professionally or personally. This is also a great time to connect your mentorship with the accomplishment of your OKRs, if it helped you achieve results.
1. Add your mentorship to your resume or to your LinkedIn profile. For mentors, this might look like a separate entry to demonstrate leadership. For mentees, you might mention the mentorship in relation to the goals you achieved.
1. Share your mentorship publicly. Consider writing a blog post or LinkedIn post with a shout out to your mentor/mentee, highlighting what you learned and maybe what surprised you.

#### Other tools for async mentorship

1. [Template for using Google Docs](https://docs.google.com/document/d/1isANsPewF44w9cLP6JHZJEX4n4cyXHjqpfVvhqQKnOk/edit?usp=sharing)
1. Slack voice memos
1. Slack recording
1. Loom recording


### Career Development

Career development is a major driver of mentee goals during mentorship. Review GitLab resources for career development in our handbook:

1. Self-paced training available in GitLab Learn as part of 2021-08 Skill of the Month on [career development](https://gitlab.edcast.com/channel/skill-of-the-month-fy22)
1. [Handbook documentation](/handbook/people-group/learning-and-development/career-development/) of GitLab career development
1. How to create and use an [individual growth plan](/handbook/people-group/learning-and-development/career-development/#individual-growth-plan)
1. Use GitLab epics to [track and outline career development goals](/handbook/people-group/learning-and-development/career-development/#use-gitlab-epics-to-track-your-career-development)

### Additional discussion resources

These resources are meant to provide both mentors and mentees with additional personal and professional development. Consider reviewing these resources asynchronously and discuss/debrief them during a session with your mentor/mentee.

***Why the Power of Mentoring can Change the World***
<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/u4kTlK5mUHc" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->




